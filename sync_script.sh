#!/bin/bash

file_directory="/tmp/test/files"   # Путь к директории с файлами

log_file="/tmp/test/script-log/sync.log"   # Путь к лог  файлу

archive_dir="/tmp/test/archives"     # Путь к директории с архивами

latest_file=$(find "$file_directory" -type f -mtime -7 | sort -n | tail -n 1) # Находим последний файл, не старше 7 дней

if [ -n "$latest_file" ]; then
  # Получаем информацию о файле
  file_name=$(basename "$latest_file")
  file_size=$(du -h "$latest_file" | awk '{print $1}')

  # Записываем информацию в лог-файл
  current_date=$(date +"%Y-%m-%d %H:%M:%S")
  echo "Дата: $current_date" >> "$log_file"
  echo "Путь: $latest_file" >> "$log_file"
  echo "Имя файла: $file_name" >> "$log_file"
  echo "Размер файла: $file_size" >> "$log_file"

  # Создаем архив с файлом
  archive_name="${file_name%.*}_$(date +"%Y%m%d%H%M%S").tar.gz"
  tar -czf "$archive_directory/$archive_name" "$latest_file"

  # Перемещаем архив в директорию с архивамиcd
  if [ -f "$archive_directory/$archive_name" ]; then
    mv "$archive_directory/$archive_name" "$archive_directory"

    # Удаляем исходный файл
    rm "$latest_file"

    echo "Успешно перемещено и удалено."
  else
    echo "Ошибка при перемещении архива."
  fi
else
  echo "Нет файлов, удовлетворяющих условию."
fi
